export const mapMovie = (movie) => {
  return {
    title: movie.Title,
    year: movie.Year,
    link: `https://www.imdb.com/title/${movie.imdbID.trim()}/`,
    poster: movie.Poster,
    genre: movie.Genre,
    rating: movie.Rating,
  };
};
