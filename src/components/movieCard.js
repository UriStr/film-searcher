const movieTemplate = document.createElement('template');
movieTemplate.innerHTML = `

<link rel="stylesheet" type="text/css" href="css/style.css">


<div class="resultContent_item">

    <a href="" class="resultContent_item_link" target="_blank"></a>
    
    <img class="item_itemPoster" src="" alt="itemPoster">
    
    <div class="resultContent_item__attributeContainer">
    
        <div class="attributeContainer_genreAndYear mainFont fontGenreYear">
            <span class="attributeContainer_genre"></span>
            <span class="attributeContainer_year"></span>
        </div>
        
        <span class="attributeContainer_itemName mainFont fontItemName"></span>
        
        <div class="attributeContainer_rate mainFont fontRate">
            <img class="rate_rateIcon" src="" alt="rateIcon">
            <span class="rate_rateNumber"></span>
        </div>
        
        <div class="attributeContainer_stub">
            <span></span>
            <span></span>
        </div>
    
    </div>
</div>
`;

const params = ['title', 'poster', 'link', 'year', 'genre', 'rating'];
const mirror = (params, element) => {
  params.forEach((param) => {
    Object.defineProperty(element, param, {
      get() {
        return this.getAttribute(param);
      },
      set(value) {
        this.setAttribute(param, value);
      },
    });
  });
};

class MovieCard extends HTMLElement {
  constructor() {
    super();

    const shadow = this.attachShadow({ mode: 'open' });
    const template = movieTemplate.content.cloneNode(true);

    shadow.appendChild(template);
    mirror(params, this);
  }

  static get observedAttributes() {
    return params;
  }

  attributeChangedCallback(param, oldValue, newValue) {
    switch (param) {
      case 'title':
        this.shadowRoot.querySelector(
          '.attributeContainer_itemName'
        ).textContent = newValue;
        break;

      case 'poster':
        if (newValue === 'N/A') {
          this.shadowRoot
            .querySelector('.resultContent_item')
            .classList.add('itemNoPoster');
        } else {
          this.shadowRoot
            .querySelector('.resultContent_item')
            .classList.add('itemFull');
        }
        this.shadowRoot.querySelector('.item_itemPoster').src = newValue;
        break;

      case 'link':
        return (this.shadowRoot.querySelector(
          '.resultContent_item_link'
        ).href = newValue);

      case 'year':
        return (this.shadowRoot.querySelector(
          '.attributeContainer_year'
        ).textContent = newValue);

      case 'rating':
        const rateIconContainer = this.shadowRoot.querySelector(
          '.rate_rateIcon'
        );

        switch (true) {
          case newValue > 8:
            rateIconContainer.src = 'img/rate_icons/thumb_up.png';
            break;
          case newValue > 6:
            rateIconContainer.src = 'img/rate_icons/ok.png';
            break;
          case newValue > 4:
            rateIconContainer.src = 'img/rate_icons/thumb_down.png';
            break;
          case newValue > 2:
            rateIconContainer.src = 'img/rate_icons/sick.png';
            break;
          default:
            rateIconContainer.src = 'img/rate_icons/shit.png';
        }

        return (this.shadowRoot.querySelector(
          '.rate_rateNumber'
        ).textContent = newValue);

      case 'genre':
        return (this.shadowRoot.querySelector(
          '.attributeContainer_genre'
        ).textContent = newValue);
    }
  }
}

customElements.define('movie-card', MovieCard);
