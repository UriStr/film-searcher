import { clearNode } from '../helpers/clearContainer.js';
import { getDeclension } from '../helpers/getDeclension.js';

// Debounce function
const debounce = (func, wait, immediate) => {
  let timeout;

  return function executedFunction() {
    const context = this;
    const args = arguments;

    const later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    const callNow = immediate && !timeout;

    clearTimeout(timeout);

    timeout = setTimeout(later, wait);

    if (callNow) func.apply(context, args);
  };
};
// Returns a declension that fits the amount of films found
const dMovies = getDeclension('фильм', 'фильма', 'фильмов');
// Returns 'spinner' element
const getSpinner = () => {
  const spinner = document.createElement('div');
  spinner.className = 'overlay-loader';
  spinner.innerHTML = `
    <div class="loader">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
`;
  return spinner;
};

export const createView = () => {
  // Controls
  const crossSymbol = document.querySelector(
    '.searchForm__crossSymbolContainer'
  );

  // Main container, header and footer (to dynamically assign styles and avoid artifacts while reloading page)
  const mainContainer = document.querySelector('main');
  const headerContainer = document.querySelector('header');
  const footerContainer = document.querySelector('footer');

  // Search list
  const resultsContainer = document.querySelector(
    '.searchResultContainer__resultContent'
  );
  const resultsHeadingPositive = document.querySelector(
    '.searchResultContainer_resultTitlePositive'
  );
  const resultsHeadingNegative = document.querySelector(
    '.searchResultContainer_resultTitleNegative'
  );

  // Tags list
  const searchTags = document.querySelector('.main__searchHistoryContainer');

  // Form
  const searchForm = document.querySelector('.searchContainer__searchForm');
  const searchInput = document.querySelector('#inputFieldId');
  const searchContainer = document.querySelector('.main__searchContainer');

  // AbortController and it's signal
  let abortController = new AbortController();
  let signal = abortController.signal;

  // Renderers
  // Movie cards renderer
  const renderList = async (results, loading) => {
    if (loading) {
      return;
    }

    clearNode(resultsContainer);

    if (results.length === 0) {
      if (!mainContainer.classList.contains('main-stateSearch')) {
        mainContainer.className = 'main-stateSearchNotFound';
      }
    } else {
      mainContainer.className = 'main-stateSearchLive';
    }
    const list = document.createDocumentFragment();

    results.forEach((movieData) => {
      let movie = document.createElement('movie-card');

      movie = Object.assign(movie, movieData);

      list.appendChild(movie);
    });

    clearNode(resultsContainer);
    resultsContainer.appendChild(list);
  };
  // Searchlist renderer
  const renderSearchList = (terms) => {
    if (terms.length === 0) {
      searchInput.value = '';
      mainContainer.className = 'main-stateSearch';
    }
    const list = document.createDocumentFragment();

    terms.forEach((movie) => {
      const tag = document.createElement('a');
      tag.classList.add('searchHistoryContainer_item');
      tag.classList.add('mainFont');
      tag.classList.add('fontHistory');
      tag.href = `/?search=${movie}`;
      tag.textContent = movie;
      tag.dataset.movie = movie;

      list.appendChild(tag);
    });

    clearNode(searchTags);
    searchTags.appendChild(list);
  };
  // Movies amount renderer
  const renderCount = (count) => {
    resultsHeadingPositive.textContent = `Нашли ${count} ${dMovies(count)}`;
  };
  // Error renderer
  const renderError = (error) => {
    clearNode(resultsContainer);
    mainContainer.className = 'main-stateSearchNotFound';
    resultsHeadingNegative.textContent = error;
  };

  // Events
  const onSearchSubmit = (_listener) => {
    const listener = (event) => {
      event.preventDefault();
      if (!searchInput.value.match(/^\s*$/)) {
        abortController.abort();
        abortController = new AbortController();
        signal = abortController.signal;
        mainContainer.className = 'main-stateSearchLive';
        clearNode(resultsContainer);
        resultsContainer.appendChild(getSpinner());
        _listener(searchInput.value.toLowerCase());
      }
      searchInput.value = '';
    };

    searchForm.addEventListener('submit', listener);

    return () => searchForm.removeEventListener('submit', listener);
  };
  const onTagClick = (_listener) => {
    const listener = (event) => {
      if (
        event.target.classList.contains('searchHistoryContainer_item') &&
        !event.altKey
      ) {
        event.preventDefault();
        abortController.abort();
        abortController = new AbortController();
        signal = abortController.signal;
        mainContainer.className = 'main-stateSearchLive';
        clearNode(resultsContainer);
        resultsContainer.appendChild(getSpinner());
        _listener(event.target.dataset.movie, signal);
      }
    };

    searchTags.addEventListener('click', listener);
    return () => searchTags.removeEventListener('click', listener);
  };
  const onTagRemove = (_listener) => {
    const listener = (event) => {
      if (
        event.target.classList.contains('searchHistoryContainer_item') &&
        event.altKey
      ) {
        event.preventDefault();
        _listener(event.target.dataset.movie);
        if (!searchTags.hasChildNodes()) {
          mainContainer.className = 'main-stateSearch';
        }
      }
    };

    searchTags.addEventListener('click', listener);
    return () => searchTags.removeEventListener('click', listener);
  };
  const onUserInput = (_listener) => {
    const listener = (event) => {
      event.preventDefault();
      if (!searchInput.value.match(/^\s*$/)) {
        abortController.abort();
        abortController = new AbortController();
        signal = abortController.signal;
        mainContainer.className = 'main-stateSearchLive';
        clearNode(resultsContainer);
        resultsContainer.appendChild(getSpinner());
        _listener(searchInput.value.toLowerCase(), signal);
      }
    };

    const debouncedListener = debounce(listener, 1000);
    searchInput.addEventListener('input', debouncedListener);

    return () => searchInput.removeEventListener('input', listener);
  };

  // Other listeners

  // Cross symbol press
  crossSymbol.addEventListener('click', () => {
    searchInput.value = '';
    searchInput.focus();
  });

  // onInputFocus onInputBlur set searchState
  searchInput.addEventListener('focus', () => {
    if (mainContainer.className === 'main-stateSearch') {
      mainContainer.className = 'main-stateSearchActive';
    }
  });

  searchInput.addEventListener('blur', () => {
    if (mainContainer.className === 'main-stateSearchActive') {
      mainContainer.className = 'main-stateSearch';
    }
  });

  // Show hidden elems after reloading page (to dynamically assign styles and avoid artifacts while reloading page)
  const makeAllVisible = () => {
    mainContainer.removeAttribute('style');
    headerContainer.removeAttribute('style');
    footerContainer.removeAttribute('style');
  };
  document.addEventListener('DOMContentLoaded', makeAllVisible);

  // Assign scroll style while scrolling
  const searchFormOffsetTop = searchInput.offsetTop;

  window.addEventListener('scroll', () => {
    if (resultsContainer.hasChildNodes()) {
      // 10 added to prevent premature scroll-state-adding (and therefore prevent input bouncing)
      if (window.pageYOffset >= searchFormOffsetTop + 10) {
        mainContainer.className = 'main-stateSearchScroll';
      } else {
        mainContainer.className = 'main-stateSearchLive';
      }
    }
  });

  return {
    renderList,
    renderCount,
    renderError,
    renderSearchList,
    onSearchSubmit,
    onTagClick,
    onTagRemove,
    onUserInput,
  };
};
