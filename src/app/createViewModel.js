export const createViewModel = (model) => {
  let state = {};
  let resultsListener = null;
  let countListener = null;
  let errorListener = null;
  let searchesListener = null;

  const update = (nextState) => {
    if (nextState.error) {
      searchesListener && searchesListener(nextState.searches);
      return errorListener && errorListener(nextState.error);
    }

    searchesListener && searchesListener(nextState.searches);
    countListener && countListener(nextState.count);
    resultsListener && resultsListener(nextState.results, nextState.loading);

    state = nextState;
  };

  return {
    bindError: (listener) => (errorListener = listener),
    bindCount: (listener) => (countListener = listener),
    bindResults: (listener) => (resultsListener = listener),
    bindSearches: (listener) => (searchesListener = listener),
    handleSearchSubmit: (searchTerm, signal) =>
      model.search(searchTerm, signal),
    handleTagClick: (searchTerm, signal) => model.search(searchTerm, signal),
    handleTagRemove: (searchTerm) => model.removeTag(searchTerm),
    handleUserInput: (searchTerm, signal) => model.search(searchTerm, signal),
    init: () => {
      update(model.getState());
      model.subscribe(update);
    },
  };
};
