import { createStore } from '../helpers/createStore.js';
import { mapMovie } from '../helpers/mapMovie.js';

const localCache = new Map();

export const createModel = () =>
  createStore(
    localStorage.getItem('lastState') !== null
      ? JSON.parse(localStorage.getItem('lastState'))
      : {
          count: 0,
          results: [],
          error: false,
          searches: [],
          loading: false,
        },
    (store) => ({
      search: async (currentState, searchTerm, signal) => {
        store.setState({
          loading: true,
          count: 0,
          results: [],
          error: false,
          searches: [searchTerm].concat(
            currentState.searches.filter((term) => term !== searchTerm)
          ),
        });
        console.log(localCache);

        if (localCache.has(searchTerm)) {
          return localCache.get(searchTerm);
        }

        try {
          let dataNoRateAndGenre = await fetch(
            `http://www.omdbapi.com/?type=movie&apikey=d85693a7&s=${searchTerm}`,
            { signal }
          );

          dataNoRateAndGenre = await dataNoRateAndGenre.json();

          let dataWithRateAndGenrePromises;
          let dataWithRateAndGenreJSON;

          if (dataNoRateAndGenre.Search) {
            dataWithRateAndGenrePromises = dataNoRateAndGenre.Search.map(
              async (obj) => {
                let result = await fetch(
                  `http://www.omdbapi.com/?i=${obj.imdbID.trim()}&apikey=d85693a7`,
                  { signal }
                );
                result = await result.json();
                obj.Genre = result.Genre;
                obj.Rating = result.imdbRating;
                return obj;
              }
            );

            dataWithRateAndGenreJSON = await Promise.all(
              dataWithRateAndGenrePromises
            );
          }

          if (dataNoRateAndGenre.Response !== 'True') {
            return {
              loading: false,
              error: dataNoRateAndGenre.Error.includes('not found')
                ? 'Мы не поняли о чем речь ¯\\_(ツ)_/¯'
                : dataNoRateAndGenre.Error.includes('many results')
                ? 'Слишком много совпадений. Уточните поиск.'
                : dataNoRateAndGenre.Error,
            };
          } else {
            const outObject = {
              loading: false,
              count: dataNoRateAndGenre.totalResults,
              results: dataWithRateAndGenreJSON.map(mapMovie),
            };

            localCache.set(searchTerm, outObject);

            return outObject;
          }
        } catch (error) {
          if (error.name !== 'AbortError') {
            if (error.name === 'TypeError') {
              return {
                error: 'Проверьте настройки соединения (◕‿◕)',
                loading: false,
              };
            }
            return { error: error.name, loading: false };
          }
        }
      },
      removeTag: (currentState, searchTerm) => {
        return {
          searches: currentState.searches.filter((term) => term !== searchTerm),
        };
      },
    })
  );
